import React from "react";
import Navb from "./components/Nav";
import Start from "./components/Start";
import About from "./components/About";
import Project from "./components/Project";
import Gall from "./components/Gallery";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router className="cont">
        <Route exact={true} path="/">
          <Navb name="" />
        </Route>
        <Route path="/about">
          <Navb name=": ABOUT" />
        </Route>
        <Route path="/projects">
          <Navb name=": PROJECTS" />
        </Route>
        <Route path="/gallery">
          <Navb name=": GALLERY" />
        </Route>
        <main>
          <Route exact={true} path="/" component={Start} />
          <Route path="/about" component={About} />
          <Route path="/projects" component={Project} />
          <Route path="/gallery" component={Gall} />
        </main>
      </Router>
    </div>
  );
}

export default App;
