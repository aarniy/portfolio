import React from "react";
import "./project.css";
import Gallery from "react-grid-gallery";

import Atlantis from "./../Gallery/gallery-asset/atlantis.png";
import Atlantist from "./../Gallery/gallery-asset/atlantist.png";
import Car from "./../Gallery/gallery-asset/car2.png";
import Cart from "./../Gallery/gallery-asset/car2t.png";
import Desert from "./../Gallery/gallery-asset/desert.png";
import Desertt from "./../Gallery/gallery-asset/desertt.png";
import Duud from "./../Gallery/gallery-asset/duud.png";
import Duudt from "./../Gallery/gallery-asset/duudt.png";
import Eye from "./../Gallery/gallery-asset/eye.png";
import Eyet from "./../Gallery/gallery-asset/eyet.png";
import Driver from "./../Gallery/gallery-asset/driver.png";
import Drivert from "./../Gallery/gallery-asset/drivert.png";

const project = () => {
    const GAME = [
      {
        src: Atlantis,
        thumbnail: Atlantist,
        caption: "Atlantis",
      },
      {
        src: Duud,
        thumbnail: Duudt,
        caption: "Characters",
      },
      {
        src: Eye,
        thumbnail: Eyet,
        caption: "Eye of the Sahara",
      },
      {
        src: Driver,
        thumbnail: Drivert,
        caption: "Another character",
      },
      {
        src: Desert,
        thumbnail: Desertt,
        caption: "Desert",
      },
      {
        src: Car,
        thumbnail: Cart,
        caption: "",
      },
    ];
    return (
      <div className="project">
        <div className="smallBloc narrow">
          <h1>Personal projects</h1>
          <div className="divider" />
          <h2>Tool for saving google maps -links</h2>
          <p>
            I use google street view -imagery as drawing reference material
            quite often. I'd been accumulating lots of bookmarked google maps
            -links but had no intuitive way to browse or search them, so I made
            a tool that saves the maps -links in a database and shows them using
            OpenStreetMap. 
          </p>
          <p>This is a currently ongoing project, which I'm writing my thesis on.</p>
          <p>Uses React.js, Leaflet and Firebase REST API</p>
          <h2>Discord bot</h2>
          <p>
            A discord bot that sends randomized art prompts.
          </p>
          <p>Uses javascript, Discord.js</p>
          <div className="divider" />
          <h1>Internships</h1>
          <div className="divider" />
          <h2>Wimmalab</h2>
          <p>
            For the summer of 2018 I took part in Wimmalab, a sort of "summer
            factory" where students do a variety of projects with local tech
            companies. I was part of the Pengwin Media -team. We were tasked
            with creating the Wimmalab website, and taking care of graphics and
            web design needs of the other teams. I was also making graphics and
            character design for another team's project there.
          </p>
          <p>
            <a className="bigLink" href="http://www.wimmalab.org/">
              Wimmalab -website
            </a>
          </p>
          <h2>Preoni Oy</h2>
          <p>
            I did a 5 month internship at Preoni, an advertising agency in
            Jyväskylä. My tasks included web design, programming and graphic
            design.
          </p>
          <div className="divider" />
          <h1>School projects</h1>
          <div className="divider" />
          <h2>Third year project</h2>
          <p>
            For our third year project we made a dashboard that shows a summary
            of facebook and google analytics data, using Supermetrics' connectors, google sheets,
            google data studio and React.js. Here's a small demo of it:
          </p>
          <div className="mobileIframeRemover">
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/dYyVvpUkhyE"
              frameborder="0"
              title="project"
            />
          </div>
          <div className="mobile">
            <a className="bigLink" href="https://youtu.be/dYyVvpUkhyE">
              Go to youtube
            </a>
          </div>
          <div className="divider" />
          <h2>Mobile project</h2>
          <p>I participated in the Android Development, Mobile Application Development and Mobile Project -courses. This is one of the projects we made there using Corona SDK and lua programming:</p>
          <div className="mobileIframeRemover">
            <iframe
              width="560"
              height="315"
              src="https://www.youtube.com/embed/xd1Hq6oXr04"
              frameborder="0"
              title="shootergame"
            />
          </div>
          <div className="mobile">
            <a className="bigLink" href="https://youtu.be/xd1Hq6oXr04">
              Go to youtube
            </a>
          </div>

          <div className="divider" />

          <h2>Graphic design -course project</h2>
          <p>
            A print-ready booklet I made about the software Hexels, outlining
            its core features. I have no affiliation with the company but the
            software's idea was interesting.
          </p>
          <a
            className="bigLink"
            href="https://student.labranet.jamk.fi/~K8406/pdf/hexels.pdf"
          >
            View the .pdf
          </a>
          <div className="divider" />
          <h2>Game project</h2>
          <p>In our game development module we made a Tomb Raider -style game using Unity. I mostly made 3d models and story slides for it.</p>
          <Gallery images={GAME} enableImageSelection={false} />
        </div>
      </div>
    );
}

export default project;
