import React from "react";
import "./about.css";
import Profile from "./../../asset/profilecrop.png";
import Progress from "./../UI/Progress";

const about = () => {
    return (
      <div className="me">
        <div className="narrow">
          <img src={Profile} alt="profile" />
          <div className="smallBloc">
            <h1>About</h1>
            <p>
              Welcome to my portfolio! I'm a media engineering student at JAMK
              University of applied sciences in Jyväskylä.
            </p>
            <p className="smol">tel: 0400420386</p>
            <p className="smol">email: k8406@student.jamk.fi</p>

            <div className="divider" />
            <h2>Skills</h2>

            <div className="skills">
              <p>Javascript</p>
              <p>React.js</p>
              <p>Redux / React-Redux</p>
              <p>MySQL</p>
              <p>Firebase</p>
              <p>Google Data Studio</p>
              <p>Blender</p>
              <p>Android Studio</p>
              <p>C#</p>
              <p>Laravel</p>
              <p>
                Adobe software (Illustrator, Photoshop, Premiere, Aftereffects,
                InDesign)
              </p>
            </div>
            <div className="divider" />
            <div className="skills">
              <h2>Interests</h2>
              <p>
                Drawing and painting, both digital and traditional
              </p>
              <p>Fixed gear bicycles</p>
              <p>Architecture</p>
              <p>Books</p>
              <p>Dungeons & Dragons</p>
              <p>Driving sims</p>
            </div>
            <div className="divider" />
            <div className="prog-cont">
              <Progress radius={50} stroke={7} progress={100} skill="Finnish" />
              <Progress radius={50} stroke={7} progress={95} skill="English" />
              <Progress radius={50} stroke={7} progress={50} skill="Swedish" />
            </div>
          </div>
        </div>
      </div>
    );
}

export default about;
