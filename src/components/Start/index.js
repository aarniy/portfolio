import React from "react";
import Linkedin from "./../../asset/LI-In-Bug.png";
import Gitlab from "./../../asset/gitlab-icon-1-color-black-rgb.svg";
import Phone from "./../../asset/phone.svg";
import Email from "./../../asset/email.svg";
import classes from './Start.module.css';

const start = () => (
  <div className={classes.Start}>
    <h1 className={classes.bigTitle}>Hi! I'm Aarni Ylhäinen.</h1>
    <h3>Web Developer | Graphic designer | Programmer</h3>
    <p />
    <p>
      <a href="https://www.linkedin.com/in/aarniy/">
        <img src={Linkedin} className={classes.smallLogos} alt="linkedin" />
      </a>
      <a href="https://gitlab.com/aarniy">
        <img src={Gitlab} className={classes.smallLogos} alt="gitlab" />
      </a>
      <a href="tel:+358400420386">
        <img src={Phone} className={classes.smallLogos} alt="Phonenumber" />
      </a>
      <a href="mailto:k8406@student.jamk.fi">
        <img src={Email} className={classes.smallLogos} alt="Mail" />
      </a>
    </p>
  </div>
)

export default start;