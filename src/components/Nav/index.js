import React, { Component } from "react";
import "./nav.css";
import Logo from "./../../asset/logo.svg";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

class Navb extends Component {
  state = {
    hideBar: true,
    pageTitle: this.props.name,
  };

  componentDidMount() {
    window.onscroll = () => {
      if (window.pageYOffset === 0) {
        this.setState({
          hideBar: true,
        });
      } else {
        this.setState({
          hideBar: false,
        });
      }
    };
  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  render() {
    let navHide = this.state.hideBar ? { background: 0 } : {};
    return (
      <div className="navbar" style={navHide}>
        <div className="left-nav">
          <a href="/" className="topnavElement titleHelper">
            <img src={Logo} alt="logo" />
            <Router>
              <Route exact={true} path="/" />
            </Router>
          </a>
          <div className="topnavElement titleTitle hidemobile">{this.state.pageTitle}</div>
        </div>
        <div className="right-nav">
          <NavLink to="/" exact className="topnavElement hidemobile">
            Home
          </NavLink>
          <NavLink to="/about" exact className="topnavElement">
            About me
          </NavLink>
          <NavLink to="/projects" exact className="topnavElement">
            Projects
          </NavLink>
          <NavLink to="/gallery" exact className="topnavElement">
            Gallery
          </NavLink>
        </div>
      </div>
    );
  }
}

export default Navb;
