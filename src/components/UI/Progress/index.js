import React, { Component } from "react";
import "./progress.css";

class Progress extends Component {
  constructor(props) {
    super(props);

    const { radius, stroke } = this.props;

    this.normalizedRadius = radius - stroke * 2;
    this.circumference = this.normalizedRadius * 2 * Math.PI;
  }
  render() {
    const { radius, stroke, progress, skill } = this.props;
    const strokeDashoffset =
      this.circumference - (progress / 100) * this.circumference;
    return (
      <div className="prog">
        <svg height={radius * 2} width={radius * 2}>
          <circle
            stroke="#08ce8c"
            fill="transparent"
            strokeWidth={stroke}
            strokeDasharray={this.circumference + " " + this.circumference}
            style={{ strokeDashoffset }}
            r={this.normalizedRadius}
            cx={radius}
            cy={radius}
          />
        </svg>
        <p>{skill}</p>
      </div>
    );
  }
}
export default Progress;
