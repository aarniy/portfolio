import React from "react";
import classes from './Gallery.module.css';
import Gallery from "react-grid-gallery";
import IMAGES from "./gallery-asset/gallery-exporter.js";

const gall = () => (
  <div className={classes.Gallery}>
    <Gallery images={IMAGES} enableImageSelection={false} />
  </div>
)

export default gall;
