import Fukuoka from "./fukuoka.jpg";
import Fukuokat from "./fukuokat.jpg";
import Mito from "./mito.jpg";
import Mitot from "./mitot.jpg";
import Outokumpu from "./outokumpu.jpg";
import Outokumput from "./outokumput.jpg";
import Tower from "./tower.png";
import Towert from "./towert.png";
import Sado from "./sadod_rev.jpg";
import Sadot from "./sadod_revt.jpg";
import Corolla from "./corolla.jpg";
import Corollat from "./corollat.jpg";
import Awaji from "./awaji.jpg";
import Awajit from "./awajit.jpg";
import r30 from "./r30.jpg";
import r30t from "./r30t.jpg";
import leclerc from "./leclerc.jpg";
import leclerct from "./leclerct.jpg";
import Atlantis from "./atlantis.png";
import Atlantist from "./atlantist.png";
import Car from "./car2.png";
import Cart from "./car2t.png";
import Desert from "./desert.png";
import Desertt from "./desertt.png";
import Duud from "./duud.png";
import Duudt from "./duudt.png";
import Eye from "./eye.png";
import Eyet from "./eyet.png";
import Driver from "./driver.png";
import Drivert from "./drivert.png";
import Shopping from "./shopping.jpg";
import Field from './fields.jpg';
import Fieldt from './fieldst.jpg';

const IMAGES = [
    {
      src: Mito,
      thumbnail: Mitot,
    },
    {
      src: Fukuoka,
      thumbnail: Fukuokat,
    },
    {
      src: Field,
      thumbnail: Fieldt,
    },
    {
      src: Sado,
      thumbnail: Sadot,
    },
    {
      src: Corolla,
      thumbnail: Corollat,
    },
    {
      src: Awaji,
      thumbnail: Awajit,
    },
    {
      src: r30,
      thumbnail: r30t,
    },
    {
      src: leclerc,
      thumbnail: leclerct,
    },
    {
      src: Atlantis,
      thumbnail: Atlantist,
    },
    {
      src: Duud,
      thumbnail: Duudt,
    },
    {
      src: Eye,
      thumbnail: Eyet,
    },
    {
      src: Driver,
      thumbnail: Drivert,
    },
    {
      src: Desert,
      thumbnail: Desertt,
    },
    {
      src: Car,
      thumbnail: Cart,
    },
    {
      src: Tower,
      thumbnail: Towert,
    },
    {
      src: Shopping,
      thumbnail: Shopping,
    },
    {
      src: Outokumpu,
      thumbnail: Outokumput,
    },
  ];

export default IMAGES;